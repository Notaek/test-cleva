# Test pour Cleva

Ce test consiste en la réalisation d'un petit projet répondant au besoin suivant :

_Un héros s'aventurait dans un monde dangereux,
Frayant son passage dans les bois obscurs._

## Description
Il s'agit de modéliser les déplacements d'un personnage sur une carte.

## Carte
La carte est un fichier txt encodé en UTF-8 présent dans un fichier "carte.txt" disponible dans les ressources du projet.

## Légende
#= Bois impénétrables.

[ ] (caractère espace) : case où le personnage peut se déplacer.

## Déplacement du personnage
Les déplacements du personnage sont définis par un fichier avec les caractéristiques suivantes :
-	encodage: UTF-8
-	Première ligne :
- -	Contient les coordonnées initiales du personnage sous la forme "x,y"
- -	Les coordonnées (0,0) correspondent au coin supérieur gauche de la carte
- Deuxième ligne :
- -	Les déplacements du personnage définis sous la forme d'une succession de caractères représentant les directions cardinale (N,S,E,O)
- - Chaque caractère correspond au déplacement d'une case

## Interactions avec les éléments de la carte 
Le personnage ne peut pas aller au-delà des bords de la carte.

Le personnage ne peut pas  aller dans les bois.