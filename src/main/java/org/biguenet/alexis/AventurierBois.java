package org.biguenet.alexis;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Stream;

public class AventurierBois {

    private static Logger logger = LogManager.getLogger(AventurierBois.class);
    private List<String[]> map = new ArrayList<>();
    private String instructions;
    private int heroX;
    private int heroY;

    /**
     * Lance l'aventure
     */
    public void startAdventure() {
        try {
            map = getMapAsArray();
            setupInstructions();
            map.get(heroY)[heroX] = "H";

            System.out.printf("Le héros commence à la position %s, %s%n", heroX, heroY);

            for (int i = 0; i <= instructions.length()-1; i++) {
                moveHero(i);
            }
            displayParcours();
            System.out.printf("Le héros termine à la position %s, %s%n", heroX, heroY);
        }
        catch (FileNotFoundException e) {
            logger.error(e);
        }
    }

    /**
     * Permet de bouger le héros en mettant à jour là où il est (et laissant là où il est passé)
     * @param index : l'instruction à lire.
     */
    public void moveHero(int index) {
        char direction = instructions.charAt(index);
        if(isMoveLegal(direction)) {
            switch(direction) {
                case 'N':{
                    heroY--;
                    break;
                }
                case 'S': {
                    heroY++;
                    break;
                }
                case 'E': {
                    heroX++;
                    break;
                }
                case 'W': {
                    heroX--;
                    break;
                }
            }
            map.get(heroY)[heroX] = "H";
        }
    }

    /**
     * Lit dans les ressources un fichier "carte.txt" et retourne une liste de tableau de String.
     * @return data : la carte représentant le fichier "carte.txt" pris en entrée.
     * @throws FileNotFoundException : La carte n'existe pas ou elle est mal nommée.
     */
    public List<String[]> getMapAsArray() throws FileNotFoundException {
        List<String[]> data = new ArrayList<>();
        try {
            FileInputStream fis = new FileInputStream("src/main/resources/carte.txt");
            String dataString = IOUtils.toString(fis, StandardCharsets.UTF_8);

            //On ne garde que les caractères prévus # et " " et on les split
            Stream<String> dataLines = dataString.lines();
                            dataLines.forEach(line -> {
                                line = line.replaceAll("[^# ]", "");
                                data.add(line.split("(?!^)"));
                            });
        }

        catch (FileNotFoundException fileNotFoundException) {
            throw new FileNotFoundException("La carte \"carte.txt\" n'existe pas ou n'a pas été trouvée");
        } catch (IOException e) {
            logger.error(e);
            throw new RuntimeException(e);
        }
        return data;
    }

    /**
     * Permet de récupérer les informations dans le fichier "instructions.txt" afin de déterminer la position de départ du héros et les instructions qu'il doit suivre.
     * @throws FileNotFoundException : Le fichier "instructions.txt" n'existe pas ou est mal nommé.
     */
    public void setupInstructions() throws FileNotFoundException {
        try {
            FileInputStream fis = new FileInputStream("src/main/resources/instructions.txt");
            String dataString = IOUtils.toString(fis, StandardCharsets.UTF_8);

            /*On suppose que la 1ere ligne = la position du héros
            2eme ligne = instructions
            On ignorera les autres
             */
            Stream<String> dataLines = dataString.lines();
            List<String> config = new ArrayList<>(dataLines.toList());

            //Récupération de la position du héros
            String heroData = config.get(0);
            heroData = heroData.replaceAll("[^0-9]","");
            heroX = Integer.parseInt(String.valueOf(heroData.charAt(0)));
            heroY = Integer.parseInt(String.valueOf(heroData.charAt(1)));

            //Récupération des instructions
            config.set(1, config.get(1).replaceAll("[^NSEO]", ""));
            instructions = config.get(1);

        } catch (FileNotFoundException fileNotFoundException) {
            throw new FileNotFoundException("Les instructions contenues dans \"instructions.txt\" n'existent pas ou le fichier n'a pas été trouvé");
        } catch (IOException e) {
            logger.error(e);
            throw new RuntimeException(e);
        } catch (IndexOutOfBoundsException e) {
            logger.error("Le fichier d'instructions n'est pas correctement configuré. " +
                    "Il doit comprendre une première ligne avec la position X,Y du héros et une " +
                    "deuxième ligne avec les instructions de déplacement.");
            throw new RuntimeException(e);
        }

    }


    /**
     * Affiche le parcours du héros, représenté par les H.
     */
    public void displayParcours() {
        map.forEach(line -> {
            System.out.printf(Arrays.toString(line) + "\n");
        });
    }

    /**
     * Détermine si une direction prise par le héros est légale (aka si elle dépasserait la carte ou atterrirait dans les bois interdits)
     * @param direction : la direction dans laquelle le héros se dirige (NSEO)
     * @return true si le mouvement est légal, sinon false.
     */
    private boolean isMoveLegal (char direction){
        boolean isMoveLegal = true;
        switch (direction) {
            case 'N':{
                if(heroY == 0 || map.get(heroY - 1)[heroX].equals("#")){
                    isMoveLegal = false;
                }
                break;
            }
            case 'S': {
                if(heroY == map.size()-1 || map.get(heroY + 1)[heroX].equals("#")){
                    isMoveLegal = false;
                }
                break;
            }
            case 'E': {
                if(heroX == map.get(heroY).length-1 || map.get(heroY)[heroX + 1].equals("#")) {
                    isMoveLegal = false;
                }
                break;
            }
            case 'O': {
                if(heroX == 0 || map.get(heroY)[heroX - 1].equals("#")) {
                    isMoveLegal = false;
                }
                break;
            }
            default:
                logger.error(String.format("La direction %s est invalide", direction));
                break;
        }
        return isMoveLegal;
    }
}
